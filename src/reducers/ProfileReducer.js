import { profileTypes} from '../constants/ActionTypes'
import { combineReducers } from 'redux'

const initialStateInputs = {
    email: '',
    actualBalance: '',
    availableBalance: '',
    address: '',
    isShowInfor: false,
    isLogout: false,
    isSelectDrop: 1,
}

const initialStateSent =  {
    addressTo:'',
    amount:'',
    isShow: false,
    isValidAmount: true,
}

const initStateTransaction = {
    transaction: [],
    page_cur: 1,
    total_page:1,

}
const transaction = (state = initStateTransaction, action) => {
    switch(action.type){
        case profileTypes.SET_LIST_TRANSACTION:
            return Object.assign({}, state, {transaction: action.transaction})
        case profileTypes.SET_PAGE_CURRENT:
            return Object.assign({}, state, {page_cur: action.value})
        case profileTypes.SET_TOTAL_PAGE:
            return Object.assign({}, state, {total_page: action.value})
        case profileTypes.RESET_TRANSACTION:
            return Object.assign({}, initStateTransaction)
        default:
            return state;
    }
}

const inputs = (state = initialStateInputs, action) => {
    switch(action.type){
        case profileTypes.SET_EMAIL_PROFILE:
            return Object.assign({}, state, {
                email: action.email
            });
        case profileTypes.SET_ACTUAL_BALANCE:
            return Object.assign({}, state, {
                actualBalance: action.actualBalance
            });
        case profileTypes.SET_AVAILABLE_BALANCE:
            return Object.assign({}, state, {
                availableBalance: action.availableBalance
            });
        case profileTypes.SET_ADDRESS:
            return Object.assign({}, state, {
                address: action.address
            });
        case profileTypes.OPEN_SHOW_INFOR:
            return Object.assign({}, state, {
                isShowInfor: true
            });
        case profileTypes.CLOSE_SHOW_INFOR:
            return Object.assign({}, state, {
                isShowInfor: false
            });
        case profileTypes.SET_LOGOUT:
            return Object.assign({}, state, {
                isLogout: action.mstatus
            });
        case profileTypes.RESET_LOGOUT:
            return Object.assign({}, state, {
                isLogout: false
            });
        case profileTypes.SET_TYPE_TRANSACTION:
            return Object.assign({}, state, {
                isSelectDrop: action.value
            });
        default:
            return state;
    }
    
}

const formSent = (state = initialStateSent, action) => {
    switch(action.type){
        case profileTypes.SET_ADDRESS_T0:
            return Object.assign({}, state, {
                addressTo: action.addressTo
            });
        case profileTypes.SET_AMOUNT_SENT:
            return Object.assign({}, state, {
                amount: action.amount
            });
        case profileTypes.SET_DESCRIPTION:
            return Object.assign({}, state, {
                description: action.description
            });
        case profileTypes.SET_IS_SHOW_FORM_SENT:
            return Object.assign({}, state, {
                isShow: action.mstatus
            });
        case profileTypes.SET_ISVALID_AMOUNT:
            return Object.assign({}, state, {
                isValidAmount: action.mstatus
            });
        case profileTypes.RESET_FROM_SENT:
            return Object.assign({}, initialStateSent);
        default:
            return state;
    }
}

export default combineReducers({
    inputs,
    formSent,
    transaction,
})
