import { internalAddressTypes} from '../constants/ActionTypes'
import { combineReducers } from 'redux'



const initStateAddress = {
    listAddresses: [],
    page_cur: 1,
    total_page:1,
    message : '',
    balances : '',
    searchAddress : ''

}
const address = (state = initStateAddress, action) => {
    switch(action.type){
        case internalAddressTypes.SET_SEARCH_ADDRESS :
            return Object.assign({}, state,{searchAddress : action.searchAddress});
        case internalAddressTypes.GET_ALL_INTERNAL_ADDRESS_SUCCESS :
            return Object.assign({}, state, 
                {listAddresses: action.response.data, total_page : action.response.totalPage})
        case internalAddressTypes.GET_ALL_INTERNAL_ADDRESS_FAILURE:
            return Object.assign({}, state);
        case internalAddressTypes.FIND_ADDRESS_SUCCESS:
            return Object.assign({}, state, {balances: action.response})
        case internalAddressTypes.FIND_ADDRESS_FAILURE:
            return Object.assign({}, state, {message : 'Not Found',balances : ''})
        case internalAddressTypes.SET_PAGE_CUR :
            return Object.assign({}, state,{page_cur : action.pageCur});
        default:
            return state;
    }
}


export default combineReducers({
    address
})
