import {forgotPasswordTypes} from '../constants/ActionTypes'
import { combineReducers } from 'redux'
const initalState = {
    newPassword: '',
    confirm: '',
    isReset: false,
    isEnd: false,
    isValidNewPassword: true,
    isValidConfirmPassword: true,

}

const forgotPassword = (state = initalState, action) => {
    switch(action.type){
        case forgotPasswordTypes.SET_NEW_PASSWORD_FORGOT:
            return Object.assign({}, state, {
                newPassword: action.newPassword
            });
        case forgotPasswordTypes.SET_CONFIRM_PASSWORD_FORGOT:
            return Object.assign({}, state, {
                confirm: action.confirm
            });
        case forgotPasswordTypes.SET_IS_RESET_PASSWORD:
            return Object.assign({}, state, {
                isReset: action.mstatus
            })
        case forgotPasswordTypes.SET_IS_END_FORGOT_PASSWORD:
            return Object.assign({}, state, {
                isEnd: action.mstatus
            });
        case forgotPasswordTypes.SET_ISVALID_CONFIRM_PASSWORD_FORGOT:
            return Object.assign({}, state, {
                isValidConfirmPassword: action.mstatus
            });
        case forgotPasswordTypes.SET_ISVALID_NEW_PASSWORD_FORGOT:
            return Object.assign({}, state, {
                isValidNewPassword: action.mstatus
            });
        case forgotPasswordTypes.RESET_FORM_FORGOT_PASSWORD:
            return Object.assign({}, initalState);
        default: 
            return state;
    }
}

export default combineReducers({
    forgotPassword
})