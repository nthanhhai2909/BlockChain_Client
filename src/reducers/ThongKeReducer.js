import { combineReducers } from 'redux'
import {thongKeTypes} from '../constants/ActionTypes'

const init = {
    numberUser: 0,
    actualBalance: 0,
    availableBalance: 0,
    isLogout: false,
}
const ThongKe = (state = init, action) => {
    switch(action.type){
        case thongKeTypes.SET_NUMBER_USER:
            return Object.assign({}, state, {
                numberUser: action.value
            });
        case thongKeTypes.SET_AVAILABLE:
            return Object.assign({}, state, {
                availableBalance: action.value
            });
        case thongKeTypes.SET_ACTUAL:
            return Object.assign({}, state, {
                actualBalance: action.value
            });
        case thongKeTypes.SET_LOGOUT:
            return Object.assign({}, state, {
                isLogout: action.mstatus
            });
        case thongKeTypes.RESET_LOGOUT:
            return Object.assign({}, state, {
                isLogout: false
            });
        default:
            return state;
    }
}

export default combineReducers({
    ThongKe,

})

