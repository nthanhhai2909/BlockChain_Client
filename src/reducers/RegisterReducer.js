import {registerTypes} from '../constants/ActionTypes'
import { combineReducers } from 'redux'

const initialStateInput = {
    email:'',
    password:'',
    confirm: '',
    isAcceptTerm: false,
}

const initialStateIsValid = {
    isValidEmail:true,
    isValidPassword:true,
    isValidConfirm: true,
    isValidAccpetTerm: true,
}

const initialStateRegister= {
    isRegister: true,
    isShowDialog: false,
    isEnd: false,
}




const inputs = (state = initialStateInput, action) =>{

    switch(action.type){
        case registerTypes.SET_EMAIL_REGISTER:
            return Object.assign({}, state, {
                email: action.email
            });
        case registerTypes.SET_PASSWORD_REGISTER:
            return Object.assign({}, state, {
                password: action.password
            });
        case registerTypes.SET_CONFIRM_REGISTER:
            return Object.assign({}, state, {
                confirm: action.confirm
            });
        case registerTypes.SET_ACCPECT_TERM:
            return Object.assign({}, state, {
                isAcceptTerm: action.mstatus
            })
        case registerTypes.RESET_INPUT:
            return Object.assign({}, initialStateInput);
        default:
            return state;
    }
}

const isvalids = (state = initialStateIsValid, action) => {
    switch(action.type){
        case registerTypes.SET_ISVALID_EMAIL:
            return(Object.assign({}, state, {
                isValidEmail: action.mstatus
            }));
        case registerTypes.SET_ISVALID_PASSWORD:
            return(Object.assign({}, state, {
                isValidPassword: action.mstatus
            }));
        case registerTypes.SET_ISVALID_COFIRM:
            return(Object.assign({}, state,  {
                isValidConfirm: action.mstatus
            }));
        case registerTypes.SET_ISVALID_ACCPET_TERM:
            return(Object.assign({}, state,  {
                isValidAccpetTerm: action.mstatus
            }));
        case registerTypes.RESET_ISVALID:
            return Object.assign({}, initialStateIsValid);
        default:
            return state
    }
}

const register = (state = initialStateRegister, action) => {
    switch(action.type){
        case registerTypes.SET_IS_REGISTER:
            return Object.assign({}, state, {
                isRegister: action.mstatus
            });
        case registerTypes.SET_IS_SHOWDIALOG_NOTIFICATION:
            return Object.assign({}, state, {
                isShowDialog: action.mstatus
            });
        case registerTypes.SET_END: 
            return Object.assign({}, state, {
                isEnd: action.mstatus
            });
        default:
            return state;
    }
    
}

export default combineReducers({
    inputs,
    isvalids,
    register
})