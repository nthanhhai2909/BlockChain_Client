import { combineReducers } from 'redux'
import registerReducers from './RegisterReducer'
import LoginReducer from './LoginReducer'
import forgotPasswordReducer from './ForgotPasswordReducer'
import ForgotPasswordNotiReducer from './ForgotPasswordNotiReducer'
import profileReducer from './ProfileReducer'
import confirmSentReducer from './ConfirmSentReducer'
import ThongKeReducer from './ThongKeReducer'
import InternalAddressReducer from './InternalAddressReducer'

import ListUserReducer from './ListUserReducer'
import ListTransactionReducer from './ListTransactionReducer'


import TransactionManagementReducer from './TransactionManagementReducer'
export default combineReducers({
    registerReducers,
    LoginReducer,
    ForgotPasswordNotiReducer,
    forgotPasswordReducer,
    profileReducer,
    confirmSentReducer,
    ThongKeReducer,
    TransactionManagementReducer,
    InternalAddressReducer,
    ListUserReducer,
    TransactionManagementReducer,
    ListTransactionReducer
})
