import { combineReducers } from 'redux'
import {listTransactionTypes} from '../constants/ActionTypes'

const init = {
    listTransaction:[],
    page_cur: 1,
    total_page:1,
}
const ListTransaction = (state = init, action) => {
    switch(action.type){
    case listTransactionTypes.SET_LIST_TRANSACTION:
        return Object.assign({}, state, {
            listTransaction: action.value
        });
    case listTransactionTypes.SET_PAGE_CURRENT:
        return Object.assign({}, state, {
            page_cur: action.value
        });
    case listTransactionTypes.SET_TOTAL_PAGE:
        return Object.assign({}, state, {
            total_page: action.value
        });
        default:
            return state;
    }
}
export default combineReducers({
    ListTransaction,

})

