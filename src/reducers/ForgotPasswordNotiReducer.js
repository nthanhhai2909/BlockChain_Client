import {forgotPasswordNotiTypes} from '../constants/ActionTypes'
import { combineReducers } from 'redux'

const init = {
    isForGot: false,
    email: '',
}
const inputs = (state = init, action) => {
    switch(action.type){
        case forgotPasswordNotiTypes.SET_IS_FORGOTPASSWORDNOTI:
            return Object.assign({}, state, {
                isForGot: action.mstatus
            })
        case forgotPasswordNotiTypes.SET_EMAIL_FORGOT:
            return Object.assign({}, state, {
                email: action.email
            })
        default:
            return state;
    }
}

export default combineReducers({
    inputs,

})

