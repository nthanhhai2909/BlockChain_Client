import {loginTypes} from '../constants/ActionTypes'
import { combineReducers } from 'redux'

const initialStateInput = {
    email:'',
    password:'',
    isLogin: false,
}

const initialStateIsValid = {
    isValidEmail:true,
    isValidPassword:true,
}

const initialStateLogin= {
    message: '',
    
}




const inputs = (state = initialStateInput, action) =>{

    switch(action.type){
        case loginTypes.SET_EMAIL_LOGIN:
            return Object.assign({}, state, {
                email: action.email
            });
        case loginTypes.SET_PASSWORD_LOGIN:
            return Object.assign({}, state, {
                password: action.password
            });
            
        case loginTypes.RESET_INPUT_LOGIN:
            return Object.assign({}, initialStateInput);
        case loginTypes.LOGINSUCCESS:
            return(Object.assign({}, state, {
                isLogin: action.result
            }));
        default:
            return state;
    }
}

const isvalids = (state = initialStateIsValid, action) => {
    switch(action.type){
        case loginTypes.SET_ISVALID_EMAIL_LOGIN:
        return(Object.assign({}, state, {
            isValidEmail: action.mstatus
        }));
        case loginTypes.SET_ISVALID_PASSWORD_LOGIN:
        return(Object.assign({}, state, {
            isValidPassword: action.mstatus
        }));
        default:
            return state
    }
}

const Login = (state = initialStateLogin, action) => {
    switch(action.type){
        case loginTypes.MESSAGELOGIN:
            return(Object.assign({}, state, {
                message: action.result
            }));
        
        case loginTypes.RESET_VALID_LOGIN:
            return(Object.assign({}, initialStateLogin));
        default:
            return state;
    }
    
}

export default combineReducers({
    inputs,
    isvalids,
    Login
})