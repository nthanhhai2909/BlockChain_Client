import { combineReducers } from 'redux'
import {listUserTypes} from '../constants/ActionTypes'

const init = {
    listUser:[],
    page_cur: 1,
    total_page:1,
}
const ListUser = (state = init, action) => {
    switch(action.type){
    case listUserTypes.SET_LIST_USER:
        return Object.assign({}, state, {
            listUser: action.value
        });
    case listUserTypes.SET_PAGE_CURRENT:
        return Object.assign({}, state, {
            page_cur: action.value
        });
    case listUserTypes.SET_TOTAL_PAGE:
        return Object.assign({}, state, {
            total_page: action.value
        });
        default:
            return state;
    }
}
export default combineReducers({
    ListUser,

})

