import {confirmSentTypes}from '../constants/ActionTypes'
import { combineReducers } from 'redux'

const initstate = {
    email:''
}
const inputs = (state = initstate, action) => {
    switch(action.type){    

        case confirmSentTypes.SET_EMAIL_CONFIRM_SENT:
            return Object.assign({}, {email: action.email});
        default:
            return state;
    }
}

export default combineReducers({
    inputs,

})
