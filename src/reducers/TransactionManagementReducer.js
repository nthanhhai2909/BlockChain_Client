import { unconfirmTransactionTypes} from '../constants/ActionTypes'
import { combineReducers } from 'redux'



const initStateTransaction = {
    transaction: [],
    page_cur: 1,
    total_page:1,
    message : ''

}
const transaction = (state = initStateTransaction, action) => {
    switch(action.type){
        case unconfirmTransactionTypes.GET_ALL_TRANSACTION_UNCONFIRM_SUCCESS:
            return Object.assign({}, state, {transaction: action.response.data, total_page : action.response.totalPage})
        case unconfirmTransactionTypes.GET_ALL_TRANSACTION_UNCONFIRM_FAILURE:
            return Object.assign({}, state);
        case unconfirmTransactionTypes.REVOKE_TRANSACTION_SUCCESS:
            return Object.assign({}, state, {message : 'Revoke the transaction successfully!'});
        case unconfirmTransactionTypes.REVOKE_TRANSACTION_FAILURE:
            return Object.assign({}, state, {message : 'Revoke the transaction falure!'});
        case unconfirmTransactionTypes.SET_PAGE_CUR :
        return Object.assign({}, state, {page_cur : action.pageCur});
        default:
            return state;
    }
}


export default combineReducers({
    transaction
})
