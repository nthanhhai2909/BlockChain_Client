import {forgotPasswordNotiTypes} from '../constants/ActionTypes'
import axios from 'axios'
export const setIsForgetPasswordNoti = mstatus => ({
    type: forgotPasswordNotiTypes.SET_IS_FORGOTPASSWORDNOTI,
    mstatus
})

export const submitForm = () => (dispatch, getState) => {
    dispatch(setIsForgetPasswordNoti(true));
    axios.post('http://localhost:3000/user/forgotpassword', {
        email: getState().ForgotPasswordNotiReducer.inputs.email
    })
}

export const resetForm = () => (dispatch, getState) => {
    dispatch(setIsForgetPasswordNoti(false));
}


export const  setEmailReset = email => ({
    type: forgotPasswordNotiTypes.SET_EMAIL_FORGOT,
    email
})