import {loginTypes} from '../constants/ActionTypes'
import axios from 'axios'

//-------------------------------- set input ------------------------------------------
export const setEmail = email => ({
    type: loginTypes.SET_EMAIL_LOGIN,
    email
})

export const setPassword = password => ({
    type: loginTypes.SET_PASSWORD_LOGIN,
    password
})

export const messageLogin = result => ({
    type: loginTypes.MESSAGELOGIN,
    result
})
export const LoginSuccsess = (result) => ({
    type: loginTypes.LOGINSUCCESS,
    result
})
//-- set message
export const setIsValidEmail = mstatus => ({
    type: loginTypes.SET_ISVALID_EMAIL_LOGIN,
    mstatus
})
export const setIsValidPassword = mstatus => ({
    type: loginTypes.SET_ISVALID_PASSWORD_LOGIN,
    mstatus
})

export const ResetValidLogin = () => ({
    type: loginTypes.RESET_VALID_LOGIN,
    
})
export const resetInput = () => ({
    type: loginTypes.RESET_INPUT_LOGIN,
    
})

//-------------------------------- Xử lý hợp lệ input ------------------------------------------

const isValidEmail = email => {
    if(email === ''){return false;}
    if(email.indexOf('@') === -1){return false;}
    
    return true;
}

const isValidPassword = password => {
    if(password.length < 6) {return false;}
    return true;
}

//-------------------------------- xử ký message ------------------------------------------

const isValidForm = (dispatch, getState) => {
    dispatch(ResetValidLogin());

    let count = 0;
    const objInput = getState().LoginReducer.inputs;
    if(isValidEmail(objInput.email) === false){
        dispatch(setIsValidEmail(false));
        count++;
    }
    else{
        dispatch(setIsValidEmail(true));
    }

    if(isValidPassword(objInput.password) === false){
        dispatch(setIsValidPassword(false));
        count++;
    }
    else{
        dispatch(setIsValidPassword(true));
    }

 
    return count;
}

//-------------------------------- Gửi cho reducer ------------------------------------------


export const submitForm = () => (dispatch, getState) => {

    const objInput = getState().LoginReducer.inputs;
    if(isValidForm(dispatch, getState) > 0){
        return;
    }

    axios.post('http://localhost:3000/login',{
        email: objInput.email, 
        password: objInput.password,
    })
    .then((response)=>{
        if(response.data.status === 'true'){
             dispatch(LoginSuccsess(true));
            //  dispatch(reset());
        }
        else{
             dispatch(messageLogin("Email or Password invalid"));
        }
    })
}


export const reset = () => (dispatch, getState) => {
    dispatch(resetInput());
}