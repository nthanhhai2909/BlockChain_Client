import { internalAddressTypes } from '../constants/ActionTypes'
import axios from 'axios'



export const getAllInternalAddressSuccess = (response) => {
    return {
        type: internalAddressTypes.GET_ALL_INTERNAL_ADDRESS_SUCCESS,
        response
    }
}

export const getAllInternalAddressFailure = (error) => {
    return {
        type: internalAddressTypes.GET_ALL_INTERNAL_ADDRESS_FAILURE,
        error
    }
}

export const findAddressSuccess = (response) => {
    return {
        type: internalAddressTypes.FIND_ADDRESS_SUCCESS,
        response
    }
}

export const findAddressFailure = (error) => {
    return {
        type: internalAddressTypes.FIND_ADDRESS_FAILURE
    }
}

export const setSearchAddress = (searchAddress) => {
    return {
        type: internalAddressTypes.SET_SEARCH_ADDRESS,
        searchAddress
    }
}

export const setPageCur = (pageCur) => {
    return {
        type: internalAddressTypes.SET_PAGE_CUR,
        pageCur
    }
}




