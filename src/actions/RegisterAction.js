import {registerTypes} from '../constants/ActionTypes'
import axios from 'axios'

export const setEmail = email => ({
    type: registerTypes.SET_EMAIL_REGISTER,
    email
})

export const setPassword = password => ({
    type: registerTypes.SET_PASSWORD_REGISTER,
    password
})

export const setConfirm = confirm  =>({
    type: registerTypes.SET_CONFIRM_REGISTER,
    confirm
})

export const setAccpectTerm = mstatus => ({
    type: registerTypes.SET_ACCPECT_TERM,
    mstatus
})

export const changeAccpectTerm = () =>  (dispatch, getState) => {
    dispatch(setAccpectTerm(!getState().registerReducers.inputs.isAcceptTerm));
}

export const setShowDialog = mstatus => ({
    type: registerTypes.SET_IS_SHOWDIALOG_NOTIFICATION,
    mstatus
})
export const resetInput = () => ({
    type: registerTypes.RESET_INPUT
})

export const setIsValidEmail = mstatus => ({
    type: registerTypes.SET_ISVALID_EMAIL,
    mstatus
})
export const setIsValidPassword = mstatus => ({
    type: registerTypes.SET_ISVALID_PASSWORD,
    mstatus
})


export const setIsValidConfrim = mstatus => ({
    type: registerTypes.SET_ISVALID_COFIRM,
    mstatus
})

export const setIsValidAccpetTern = mstatus => ({
    type: registerTypes.SET_ISVALID_ACCPET_TERM,
    mstatus
})


export const resetIsValid = () => ({
    type:registerTypes.RESET_ISVALID
})

export const setIsRegister = mstatus => ({
    type: registerTypes.SET_IS_REGISTER,
    mstatus
})

const isValidEmail = email => {
    if(email === ''){return false;}
    if(email.indexOf('@') === -1){return false;}
    
    return true;
}

const isValidPassword = password => {
    if(password.length < 6) {return false;}
    return true;
}

const isValidCofirm = (confirm, password) => {
    if(confirm !== password){return false;}
    return true;
}

const isValidForm = (dispatch, getState) => {
    let count = 0;

    const objInput = getState().registerReducers.inputs;
    if(isValidEmail(objInput.email) === false){
        dispatch(setIsValidEmail(false));
        count++;
    }
    else{
        dispatch(setIsValidEmail(true));
    }

    if(isValidPassword(objInput.password) === false){
        dispatch(setIsValidPassword(false));
        count++;
    }
    else{
        dispatch(setIsValidPassword(true));
    }

    if(isValidCofirm(objInput.confirm, objInput.password) === false){
        dispatch(setIsValidConfrim(false));
        count++;
    }
    else{
        dispatch(setIsValidConfrim(true));
    }
    // if(objInput.isAcceptTerm === false){
    //     dispatch(setIsValidAccpetTern(false));
    //     count++
    // }
    // else{
    //     dispatch(setIsValidAccpetTern(true));
    // }
    return count;
}


export const submitForm = () => async (dispatch, getState) => {

    console.log('data')
    const objInput = getState().registerReducers.inputs;

    if(isValidForm(dispatch, getState) > 0){
        return;
    }

    const register = await axios.post('http://localhost:3000/register',{
        email: objInput.email, 
        password: objInput.password,
        confirm: objInput.confirm
    })

    if(register.data.status === 'false'){
        dispatch(setIsRegister(false));
        return;
    }

    console.log('cc')
    dispatch(setIsRegister(true));
    dispatch(setShowDialog(true));
}

export const setEnd = mstatus => ({
    type: registerTypes.SET_END,
    mstatus
})

export const reset = () => async(dispatch, getState) => {

     dispatch( resetInput());
     dispatch(resetIsValid());
     dispatch(setIsRegister(true));
     dispatch(setShowDialog(false));
    //  dispatch(setEnd(true));
}