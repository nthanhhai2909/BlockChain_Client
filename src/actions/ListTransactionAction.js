import {listTransactionTypes} from '../constants/ActionTypes'
import axios from 'axios'

export const setListTransaction = (value) => ({
    type: listTransactionTypes.SET_LIST_TRANSACTION,
    value
})
export const setPageCurrent = value => ({
    type: listTransactionTypes.SET_PAGE_CURRENT,
    value
})
export const setTotalPage = value => ({
    type: listTransactionTypes.SET_TOTAL_PAGE,
    value
})

export const loadListTransaction = (page_cur) => async (dispatch) => {
    axios.post('http://localhost:3000/admin/getTransactionUnconfirm', {
        amount: 10,
        page: page_cur
    })
    .then(async(response) => {
        if(response.data.status === 'true'){
            dispatch(setListTransaction(response.data.data)); 
            dispatch(setTotalPage(response.data.totalPage));
        }
        else{
           
        }
    })
}



export const handleKeyPress = (event)=>  async(dispatch, getState) => {
    let page_cur;
    if(event === 0)
    {
        page_cur = getState().ListTransactionReducer.ListTransaction.page_cur;
    }
    else{
        page_cur = Number(event.value);
    }
    
    if(page_cur <= getState().ListTransactionReducer.ListTransaction.total_page)
    {
        dispatch(setPageCurrent(page_cur));
    }
    dispatch(loadListTransaction(getState().ListTransactionReducer.ListTransaction.page_cur));
}



export const onclickPrev = value => (dispatch) => {
    dispatch(setPageCurrent(value - 1));  
    dispatch(handleKeyPress(0));
}
export const onclickNext = value => (dispatch) => {
    dispatch(setPageCurrent(value + 1));    
    dispatch(handleKeyPress(0));
}