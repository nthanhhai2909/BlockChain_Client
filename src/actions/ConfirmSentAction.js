import {confirmSentTypes} from '../constants/ActionTypes'
import axios from 'axios'

export const setEmail = (email) => ({
    type: confirmSentTypes.SET_EMAIL_CONFIRM_SENT,
    email
})
export const confirm = (token) => async (dispatch) => {
    let res = await axios.get('http://localhost:3000/user/confirmtransaction/' + token);
    dispatch(setEmail(res.data.email));
}