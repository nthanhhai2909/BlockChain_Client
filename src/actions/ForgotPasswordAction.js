import {forgotPasswordTypes, forgotPasswordNotiTypes} from '../constants/ActionTypes'
import axios from 'axios'
export const  setNewPassword = newPassword => ({
    type: forgotPasswordTypes.SET_NEW_PASSWORD_FORGOT,
    newPassword
})

export const setConfirmPassword = confirm => ({
    type: forgotPasswordTypes.SET_CONFIRM_PASSWORD_FORGOT,
    confirm
})

export const setIsResetPassword = mstatus => ({
    type: forgotPasswordTypes.SET_IS_RESET_PASSWORD,
    mstatus
})

export const setIsEnd = mstatus => ({
    type: forgotPasswordTypes.SET_IS_END_FORGOT_PASSWORD,
    mstatus
})

export const setIsValidNewPassword = mstatus => ({
    type: forgotPasswordTypes.SET_ISVALID_NEW_PASSWORD_FORGOT,
    mstatus
})

export const setIsValidConfirmPassword = mstatus => ({
    type: forgotPasswordTypes.SET_ISVALID_CONFIRM_PASSWORD_FORGOT,
    mstatus
})


export const resetForm = () => ({
    type: forgotPasswordTypes.RESET_FORM_FORGOT_PASSWORD
})

const isValidNewPassword = newPassword => {
    if(newPassword.length < 6){return false};
    return true;
}

const isValidConfirm = (newPassword, confirm) => {
    if(newPassword !== confirm ){return false;}
    return true;
}

const isValidForm = (dispatch, getState) => {
    var count = 0;
    if(isValidNewPassword(getState().forgotPasswordReducer.forgotPassword.newPassword)){
        dispatch(setIsValidNewPassword(true));
    }
    else{
        dispatch(setIsValidNewPassword(false));
        count++;
    }

    if(isValidConfirm(getState().forgotPasswordReducer.forgotPassword.newPassword,
                getState().forgotPasswordReducer.forgotPassword.confirm)){
        dispatch(setIsValidConfirmPassword(true));
    }
    else{
        dispatch(setIsValidConfirmPassword(false));
        count++
    }

}

export const submitForm = (email) => (dispatch, getState) => {
    if(isValidForm(dispatch, getState) > 0){
        return
    }

    axios.post('http://localhost:3000/user/resetpassword', {
        email: email,
        newPassword: getState().forgotPasswordReducer.forgotPassword.newPassword,
    })
    .then(res => {
        if(res.data.status === 'true'){
            dispatch(setIsResetPassword(true));
            dispatch(setIsEnd(true));
        }
        else {
            dispatch(setIsResetPassword(false));
        }
    })
}

