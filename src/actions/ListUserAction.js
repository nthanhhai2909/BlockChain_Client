import {listUserTypes} from '../constants/ActionTypes'
import axios from 'axios'

export const setListUser = (value) => ({
    type: listUserTypes.SET_LIST_USER,
    value
})
export const setPageCurrent = value => ({
    type: listUserTypes.SET_PAGE_CURRENT,
    value
})
export const setTotalPage = value => ({
    type: listUserTypes.SET_TOTAL_PAGE,
    value
})

export const loadListUser = (page_cur) => async (dispatch) => {
    axios.post('http://localhost:3000/admin/getinforalluser', {
        amount: 10,
        page: page_cur
    })
    .then(async(response) => {
        if(response.data.status === 'true'){
            dispatch(setListUser(response.data.data)); 
            dispatch(setTotalPage(response.data.totalPage));
        }
        else{
           
        }
    })
}



export const handleKeyPress = (event)=>  async(dispatch, getState) => {
    let page_cur;
    if(event === 0)
    {
        page_cur = getState().ListUserReducer.ListUser.page_cur;
    }
    else{
        page_cur = Number(event.value);
    }
    
    if(page_cur <= getState().ListUserReducer.ListUser.total_page)
    {
        dispatch(setPageCurrent(page_cur));
    }
    dispatch(loadListUser(getState().ListUserReducer.ListUser.page_cur));
}



export const onclickPrev = value => (dispatch) => {
    dispatch(setPageCurrent(value - 1));  
    dispatch(handleKeyPress(0));
}
export const onclickNext = value => (dispatch) => {
    dispatch(setPageCurrent(value + 1));    
    dispatch(handleKeyPress(0));
}