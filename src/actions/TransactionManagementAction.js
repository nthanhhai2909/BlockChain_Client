import {unconfirmTransactionTypes} from '../constants/ActionTypes'
import axios from 'axios'



export const getAllUnconfirmTransactionSuccess = response => {
    return {
        type : unconfirmTransactionTypes.GET_ALL_TRANSACTION_UNCONFIRM_SUCCESS,
        response
    }
}

export const getAllUnconfirmTransactionFailure = error => {
    return {
        type : unconfirmTransactionTypes.GET_ALL_TRANSACTION_UNCONFIRM_FAILURE,
        error
    }
} 

export const revokeTransactionSuccess  = ()=>{
    return {
        type : unconfirmTransactionTypes.REVOKE_TRANSACTION_SUCCESS
    }
}
export const revokeTransactionFailure = () => {
    return {
        type : unconfirmTransactionTypes.REVOKE_TRANSACTION_FAILURE
    }
}

export const setPageCur = (pageCur) =>{
    return {
        type : unconfirmTransactionTypes.SET_PAGE_CUR,
        pageCur
    }
}