import {thongKeTypes} from '../constants/ActionTypes'
import axios from 'axios'

export const setNumberUser = (value) => ({
    type: thongKeTypes.SET_NUMBER_USER,
    value
})
export const setActualBalance = (value) => ({
    type: thongKeTypes.SET_ACTUAL,
    value
})
export const setAvailableBalance = (value) => ({
    type: thongKeTypes.SET_AVAILABLE,
    value
})
export const loadNumberUser = () => async (dispatch) => {
    axios.get('http://localhost:3000/admin/alluser')
    .then(async(response) => {
        if(response.data.status === 'true'){
            dispatch(setNumberUser(response.data.data));
        }
        else{
            
        }
    })
}

export const loadActualBalance = () => async (dispatch) => {
    axios.get('http://localhost:3000/admin/getactualbanlancesystem')
    .then(async(response) => {
        if(response.data.status === 'true'){
            dispatch(setActualBalance(response.data.data));
        }
        else{
            
        }
    })
}
export const loadAvailableBalance = () => async (dispatch) => {
    axios.get('http://localhost:3000/admin/getavailablebanlancesystem')
    .then(async(response) => {
        if(response.data.status === 'true'){
            dispatch(setAvailableBalance(response.data.data));
        }
        else{
            
        }
    })
}

//--------------------------logout----------------------
export const setLogout = mstatus => ({
    type: thongKeTypes.SET_LOGOUT,
    mstatus
})
export const resetLogout = () => ({
    type: thongKeTypes.RESET_LOGOUT,
    
})
export const onClickLogout = () => async(dispatch) => {
    let logout = await axios.get('http://localhost:3000/logout');
    if(logout.data.status === 'true'){
        dispatch(setLogout(true));
    }
    else{
        
    }
}

//--------------------------end - logout----------------------