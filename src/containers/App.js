import React from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import HomeContainer from './HomeContainer'
import RegisterContainer from './RegisterContainer'
import LoginContainer from './LoginContainer'
import ConfirmEmailContainer from './ConfirmEmailContainer'
import ForgotPasswordContainer from './ForgotPasswordContainer'
import ForgotPasswordNotiContainer from './ForgotPasswordNotiContainer'
import ConfirmSentContainer from './ConfirmSentContainer'
import TransactionManagementContainer from './TransactionManagementContainer'
import '../css/index.css'
import ProfileContainer from './ProfileContainer';
import AdminContainer from './AdminContainer';
import ThongKeContainer from './ThongKeContainer';
import InternalAddressContainer from './InternalAddressContainer'
import ErrorContainer from './ErrorContainer';
import ListUserContainer from './ListUserContainer';
import ListTransactionContainer from './ListTransactionContainer';
const App = () => (
    <MuiThemeProvider>
        <Router>
            <Switch>
                <Route exact path='/' component={HomeContainer}/>
                <Route exact path='/register' component={RegisterContainer}/>
                <Route exact path='/login' component={LoginContainer}/>
                <Route exact path='/confirm/:token' component={ConfirmEmailContainer}/>
                <Route exact path='/forgot' component={ForgotPasswordNotiContainer}/>
                <Route exact path='/forgot/:email' component={ForgotPasswordContainer}/>
                <Route exact path='/profile' component={ProfileContainer}/>
                <Route exact path='/user/confirmtransaction/:token' component={ConfirmSentContainer}/>
                <Route exact path='/admin/profile' component={AdminContainer}/>
                <Route exact path='/admin/thongke' component={ThongKeContainer}/>
                <Route exact path='/admin/internal-address' component={InternalAddressContainer}/>
                <Route exact path='/admin/listuser' component={ListUserContainer}/>
                <Route exact path='/admin/listtransaction' component={ListTransactionContainer}/>
                <Route exact path='/user/transaction-management' component={TransactionManagementContainer}/>
                <Route exact path='/error' component={ErrorContainer}/>
            </Switch>
        </Router>
    </MuiThemeProvider>
)

export default App