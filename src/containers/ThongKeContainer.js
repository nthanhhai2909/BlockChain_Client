import React from 'react'
import ThongKe from '../components/AdminComponents/ThongKe'
import LayoutAdmin from '../components/AdminComponents/LayoutAdmin'
import HeaderAdmin from '../components/AdminComponents/HeaderAdmin'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import * as ThongKeAction from '../actions/ThongKeAction'
import axios from 'axios'

class ThongKeContainer extends React.Component {
    componentWillReceiveProps(nextProps) {
        if(nextProps.isLogout){
            this.props.actions.resetLogout();
            this.props.history.push('/login');

        }
    }
    async componentDidMount(){
        await axios.get('http://localhost:3000/authentication')
        .then(async(response) => {
            
            if(response.data.status === 'true' && response.data.type === 1){
                this.props.actions.loadNumberUser();
                this.props.actions.loadActualBalance();
                this.props.actions.loadAvailableBalance();
            }
            else{
                this.props.history.push('/error');
            }
        })
    }
    
    render(){
        return(
            <div>
                <HeaderAdmin
                onClickLogout={() => this.props.actions.onClickLogout()}
                />
                
                <LayoutAdmin
                />
                <ThongKe numberUser={this.props.numberUser} actualBalance={this.props.actualBalance} availableBalance={this.props.availableBalance}
                />
                
            </div>
        )
    }
}


const mapStateToProps = state => ({
    numberUser: state.ThongKeReducer.ThongKe.numberUser,
    actualBalance: state.ThongKeReducer.ThongKe.actualBalance,
    availableBalance: state.ThongKeReducer.ThongKe.availableBalance,
    isLogout: state.ThongKeReducer.ThongKe.isLogout,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(ThongKeAction, dispatch)
})

ThongKeContainer.propTypes = {
    

}

export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(ThongKeContainer)