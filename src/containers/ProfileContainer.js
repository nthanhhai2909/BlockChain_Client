import React from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import Profile from '../components/Profile'
import * as profileAction from '../actions/ProfileAction'
import axios from 'axios'
import WebSocket from 'ws'
class ProfileContainer extends React.Component {
    componentWillReceiveProps(nextProps) {
        if(nextProps.isLogout){
            this.props.actions.resetLogout();
            this.props.history.push('/login');

        }
    }
    componentDidMount(){
    
        axios.get('http://localhost:3000/authentication')
        .then(async(response) => {
            if(response.data.status === 'true'){
                this.props.actions.getAddress(response.data.user.email);
                this.props.actions.getBalance(response.data.user.email);
                // this.props.actions.handleKeyPress(0);
            }
            else{
                this.props.history.push('/login');
            }
        })
    }

    syncKcoinBlockChain(){
        const ws = new WebSocket('http://localhost:3000/', {
            origin: 'http://localhost:3000/'
        });

        ws.on('open', function open() {
            console.log('connected');
            setInterval(() => { ws.send(Date.now()); }, 30000);
    
        });

        ws.on('close', function close() {
            console.log('disconnected');
        });
    
        ws.on('message', async function incoming(data) {
            console.log('data', data);
            if(data.type === 'update_balance'){
                axios.get('http://localhost:3000/user/balance/' + this.props.email)
                .then(res => {
                    this.props.actions.setActualBalance(res.data.data.actualBalance);
                    this.props.actions.setActualBalance(res.data.data.availableBalance);
                })
            }
        });
    }
    
    render(){
        console.log('transaction', this.props.transaction)
        return(
            <div>
                <Profile
                    actualBalance={this.props.actualBalance}
                    availableBalance={this.props.availableBalance}
                    address={this.props.address}
                    isShowInfor={this.props.isShowInfor}
                    openShowInfor={() => this.props.actions.openShowInfor()}
                    closeShowInfor={() => this.props.actions.closeShowInfor()}
                    setAddressTo={(value) => this.props.actions.setAddressTo(value)}
                    setAmountSent={(value) => this.props.actions.setAmountSent(value)}
                    setDescription={(value) => this.props.actions.setDescription(value)}
                    setIsShowFromSent={(value) => this.props.actions.setIsShowFromSent(value)}
                    onclickLogout = {() => this.props.actions.onclickLogout()}
                    closeFromSent={() => this.props.actions.closeFromSent()}
                    isShow={this.props.isShow}
                    submitFormSent={() => this.props.actions.submitFormSent()}
                    transaction={this.props.transaction}
                    handleKeyPress={(event) => this.props.actions.handleKeyPress(event)}

                    isSelectDrop ={this.props.isSelectDrop}
                    handleClickTypeTransaction = {(value) => this.props.actions.handleClickTypeTransaction(value)}

                    page_cur ={this.props.page_cur}
                    total_page ={this.props.total_page}
                    onclickPrev = {(value) => {this.props.actions.onclickPrev(value)}}
                    onclickNext = {(value) => this.props.actions.onclickNext(value)}

                />
            </div>
        )
    }
}

const mapStateToProps = state => ({
    actualBalance: state.profileReducer.inputs.actualBalance,
    availableBalance:state.profileReducer.inputs.availableBalance,
    address: state.profileReducer.inputs.address,
    isShowInfor: state.profileReducer.inputs.isShowInfor,
    isShow: state.profileReducer.formSent.isShow,
    email: state.profileReducer.inputs.email,
    transaction: state.profileReducer.transaction.transaction,
    isLogout: state.profileReducer.inputs.isLogout,
    isSelectDrop: state.profileReducer.inputs.isSelectDrop,
    page_cur: state.profileReducer.transaction.page_cur,
    total_page: state.profileReducer.transaction.total_page,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(profileAction, dispatch)
})

ProfileContainer.propTypes = {
    actualBalance: PropTypes.string,
    availableBalance: PropTypes.string,
    address: PropTypes.string,
    isShowInfor: PropTypes.bool,
    isShow: PropTypes.bool,
    isLogout: PropTypes.bool,
    email: PropTypes.string,
    isSelectDrop: PropTypes.number,
    transaction: PropTypes.array

}

export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(ProfileContainer)