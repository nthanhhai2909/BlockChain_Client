import React from 'react'
import Login from '../components/Login'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import * as TransactionManagementAction from '../actions/TransactionManagementAction'
import { bindActionCreators } from 'redux'

import axios from 'axios'
import TransactionManagement from '../components/TransactionManagement';
import { debug } from 'util';
import * as ProfileAction from '../actions/ProfileAction';
axios.defaults.withCredentials = true;

class TransactionManagementContainer extends React.Component {

    componentDidMount(){
        axios.get('http://localhost:3000/authentication')
        .then(async(response) => {
            if(response.data.status === 'true'){
                console.log("day la email",  );
                const pageQuery = {
                    page : this.props.page_cur,
                    amount : 3,
                    email : response.data.user.email
                }
                axios.post('http://localhost:3000/user/transactionunconfirm/', pageQuery)
                .then((result)=>{
                    if(result.data.status === 'true'){
                        console.log("result", result);
                        this.props.actions.getAllUnconfirmTransactionSuccess(result.data);
                    }
                    else{
                        this.props.actions.getAllUnconfirmTransactionFailure(result);
                    }
                }).catch((error)=>{
                    console.log(error);
                });
            }
            else{
                this.props.history.push('/login');
            }
        })
    }
    
    
    onclickPrev(page){
        if(page > 1){
            this.props.actions.setPageCur(page - 1);
            this.componentDidMount();
        }
       
    }
    onclickNext(page){
        if(page < this.props.total_page){
            console.log("page_cur", page);
            this.props.actions.setPageCur(page + 1);
            this.componentDidMount();
        }
        
    }

    revokeTransaction(id){
        console.log("id" + id);
        axios.get('http://localhost:3000/user/delete/transactionunconfirm/' + id)
        .then((response)=>{
            console.log("sau khi delete", response);
            if(response.data.status === 'true'){
                this.props.actions.revokeTransactionSuccess();
                this.props.actions.setPageCur(1);
                this.componentDidMount();
            }
            else{
                this.props.actions.revokeTransactionFailure();
            }
        }).catch((error)=>{
            console.log("revoke")
        });
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.isLogout){
            this.props.actionLogout.resetLogout();
            this.props.history.push({
                pathname: '/login',
            })
            
        }
    }

    render(){
        
        const {transaction, page_cur, total_page} = this.props;
        console.log("day la transaction", transaction);
        return(
            <TransactionManagement 
            revokeTransaction = {(id) => this.revokeTransaction(id)}
            transaction = {transaction}
            page_cur = {page_cur}
            total_page = {total_page}
            onclickLogout = {() => this.props.actionLogout.onclickLogout()}
            onclickPrev = {(value) => {this.onclickPrev(value)}}
            onclickNext = {(value) => {this.onclickNext(value)}}
            />
        )
    }
}


const mapStateToProps = state => ({
    transaction: state.TransactionManagementReducer.transaction.transaction,
    page_cur: state.TransactionManagementReducer.transaction.page_cur,
    total_page:state.TransactionManagementReducer.transaction.total_page,
    isLogout : state.profileReducer.inputs.isLogout
})

const mapDispatchToProps = dispatch =>{
    return ({
        actions: bindActionCreators(TransactionManagementAction, dispatch),
        actionLogout : bindActionCreators(ProfileAction, dispatch)
    })
}

TransactionManagementContainer.propTypes = {
    
}
  
export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(TransactionManagementContainer)