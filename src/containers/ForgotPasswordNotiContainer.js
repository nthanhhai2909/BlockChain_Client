import React from 'react'
import ForgotPasswordNoti from '../components/ForgotPasswordNoti'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import axios from 'axios'
import * as forgotPasswordNotiAction from '../actions/ForgotPasswordNotiAction'
import { bindActionCreators } from 'redux'
class ForgotPasswordNotiContainer extends React.Component {


    backHome(){
        this.props.actions.resetForm();
        this.props.history.push('/')
    }
    render(){
        return(
            <div>
                <ForgotPasswordNoti
                    isForGot={this.props.isForGot}
                    setEmailReset={(value) => this.props.actions.setEmailReset(value)}
                    submitForm={() => this.props.actions.submitForm()}
                    resetForm={() => this.props.actions.resetForm()}
                    backHome={() => this.backHome()}
                />
            </div>
        )
    }
}

const mapStateToProps = state => ({
    email: state.LoginReducer.inputs.email,
    isForGot: state.ForgotPasswordNotiReducer.inputs.isForGot
})

const mapDispatchToProps = dispatch =>({
    actions: bindActionCreators(forgotPasswordNotiAction, dispatch)
})

ForgotPasswordNotiContainer.propTypes = {
    email: PropTypes.string.isRequired,
    isForGot: PropTypes.bool.isRequired
}

export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(ForgotPasswordNotiContainer)