import React from 'react'
import Register from '../components/Register'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import * as registerActions from '../actions/RegisterAction'
import { bindActionCreators } from 'redux'
class RegisterContainer extends React.Component {

    // componentWillReceiveProps(nextProps) {
    //     if(nextProps.isEnd){
    //         this.props.history.push('/')
    //     }
    // }

    loginSuccess(){
        this.props.actions.reset();
        this.props.history.push('/')
    }

    render(){

        const {actions} = this.props;
        return(
            <Register
                history={this.props.history}
                setEmail={(value) => actions.setEmail(value)}
                setPassword={(value) => actions.setPassword(value)}
                setConfirm={(value) => actions.setConfirm(value)}
                changeAccpectTerm={() => actions.changeAccpectTerm()}
                isvalids={this.props.isvalids}
                isRegister={this.props.isRegister}
                submitForm={() => actions.submitForm()}
                reset={() => this.loginSuccess()}
                isShowDialog={this.props.isShowDialog}
            />
        )
    }
}


const mapStateToProps = state => ({
    isvalids: state.registerReducers.isvalids,
    isRegister: state.registerReducers.register.isRegister,
    isShowDialog: state.registerReducers.register.isShowDialog,
    isEnd: state.registerReducers.register.isEnd,
})

const mapDispatchToProps = dispatch =>{
    return ({
        actions: bindActionCreators(registerActions, dispatch)
    })
}

RegisterContainer.propTypes = {
    isvalids: PropTypes.objectOf(PropTypes.bool).isRequired,
    isRegister: PropTypes.bool.isRequired,
    isShowDialog: PropTypes.bool.isRequired,
    isEnd: PropTypes.bool.isRequired
}
  
export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(RegisterContainer)