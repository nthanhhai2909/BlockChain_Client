import React from 'react'
import Login from '../components/Login'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import * as LoginAction from '../actions/LoginAction'
import { bindActionCreators } from 'redux'

import axios from 'axios'
axios.defaults.withCredentials = true;

class LoginContainer extends React.Component {

    componentDidMount(){
        axios.get('http://localhost:3000/authentication')
        .then(async(response) => {
            
            if(response.data.status === 'true'){
                
                this.props.history.push('/profile');
            }
            else{
                this.props.history.push('/login');
            }
        })
    }
    
    componentWillReceiveProps(nextProps) {
        
        if(nextProps.isLogin){
            this.props.actions.reset();
            this.props.history.push({
                pathname: '/profile',
            })

            
        }
    }

    render(){
        
        const {actions} = this.props;
        return(
            <Login
                history={this.props.history}
                setEmail={(value) => actions.setEmail(value)}
                setPassword={(value) => actions.setPassword(value)}
                isvalids={this.props.isvalids}
                message={this.props.message}
                submitForm={() => actions.submitForm()}
            />
        )
    }
}


const mapStateToProps = state => ({
    isvalids: state.LoginReducer.isvalids,
    message: state.LoginReducer.Login.message,
    isLogin: state.LoginReducer.inputs.isLogin,
    email: state.LoginReducer.inputs.email,
})

const mapDispatchToProps = dispatch =>{
    return ({
        actions: bindActionCreators(LoginAction, dispatch),
    })
}

LoginContainer.propTypes = {
    
}
  
export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(LoginContainer)