import ForgotPassword from '../components/ForgotPassword'
import React from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import * as forgotPasswordActions from '../actions/ForgotPasswordAction'
class  ForgotPasswordContainer extends React.Component {
    
    success(){
        this.props.actions.resetForm();
        this.props.history.push('/');
    }
    render(){
        return(
            <ForgotPassword
                setNewPassword={(value) => this.props.actions.setNewPassword(value)} 
                setConfirmPassword={(value) => this.props.actions.setConfirmPassword(value)}
                isValidNewPassword={this.props.isValidNewPassword}
                isValidConfirmPassword= {this.props.isValidConfirmPassword}
                isReset={this.props.isReset}
                isEnd={this.props.isEnd}
                submitForm={()  => this.props.actions.submitForm(this.props.match.params.email.slice(this.props.match.params.email.indexOf('=') + 1,
                this.props.match.params.email.length))}
                reset={() => this.success()}
            />
        )
    }
}

const mapStateToProps = state => ({
    isValidNewPassword: state.forgotPasswordReducer.forgotPassword.isValidNewPassword,
    isValidConfirmPassword: state.forgotPasswordReducer.forgotPassword.isValidConfirmPassword,
    isReset: state.forgotPasswordReducer.forgotPassword.isReset,
    isEnd: state.forgotPasswordReducer.forgotPassword.isEnd
})
const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(forgotPasswordActions, dispatch)
})
 

ForgotPasswordContainer.propTypes = {
    isValidNewPassword: PropTypes.bool.isRequired,
    isValidConfirmPassword: PropTypes.bool.isRequired,
    isReset: PropTypes.bool.isRequired,
    isEnd: PropTypes.bool.isRequired
}
  

export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(ForgotPasswordContainer) 