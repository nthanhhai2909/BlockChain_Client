import React from 'react'
import CofirmSent from '../components/ConfirmSent'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import axios from 'axios'
import { bindActionCreators } from 'redux'
import * as confirmSentAction from '../actions/ConfirmSentAction'

class ConfirmSentContainer extends React.Component {

    componentDidMount(){
        this.props.actions.confirm(this.props.match.params.token);
    }
    backhome(){
        console.log('asdasdasdasd')
        this.props.history.push({
            pathname: '/profile',
            state:{email: this.props.email}
        })

    }
    render(){
        return(
            <div>
                <CofirmSent
                backhome={() => this.backhome()}
                />
            </div>
        )
    }
}


const mapStateToProps = state => ({
    email: state.confirmSentReducer.inputs.email,
})
const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(confirmSentAction, dispatch)
})
 

ConfirmSentContainer.propTypes = {

}
  

export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(ConfirmSentContainer) 