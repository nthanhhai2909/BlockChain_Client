import React from 'react'
import LayoutAdmin from '../components/AdminComponents/LayoutAdmin'
import HeaderAdmin from '../components/AdminComponents/HeaderAdmin'
import ListUser from '../components/AdminComponents/ListUser'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import * as ListUserAction from '../actions/ListUserAction'
import * as ThongKeAction from '../actions/ThongKeAction'
import axios from 'axios'

class ListUserContainer extends React.Component {
    componentWillReceiveProps(nextProps) {
        if(nextProps.isLogout){
            this.props.actions2.resetLogout();
            this.props.history.push('/login');

        }
    }
    async componentDidMount(){
        await axios.get('http://localhost:3000/authentication')
        .then(async(response) => {
            
            if(response.data.status === 'true' && response.data.type === 1){
                this.props.actions.loadListUser(this.props.page_cur);
            }
            else{
                this.props.history.push('/error');
            }
        })
    }
    
    render(){
        return(
            <div>
                <HeaderAdmin
                onClickLogout={() => this.props.actions2.onClickLogout()}
                />
                
                <LayoutAdmin
                />
                
                <ListUser 
                page_cur = {this.props.page_cur}
                total_page = {this.props.total_page}
                listUser = {this.props.listUser}
                onclickPrev = {(value) => {this.props.actions.onclickPrev(value)}}
                onclickNext = {(value) => this.props.actions.onclickNext(value)}
                handleKeyPress={(event) => this.props.actions.handleKeyPress(event)}
                />
            </div>
        )
    }
}


const mapStateToProps = state => ({
    page_cur: state.ListUserReducer.ListUser.page_cur,
    total_page: state.ListUserReducer.ListUser.total_page,
    listUser: state.ListUserReducer.ListUser.listUser,

    isLogout: state.ThongKeReducer.ThongKe.isLogout,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(ListUserAction, dispatch),
    actions2: bindActionCreators(ThongKeAction, dispatch)
})

ListUserContainer.propTypes = {
    

}

export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(ListUserContainer)