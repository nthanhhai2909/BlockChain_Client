import React from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import Profile from '../components/Profile'
import axios from 'axios'
import InternalAddress from '../components/AdminComponents/InternalAddress';
import * as InternalAddressAction from '../actions/InternalAddressAction'
import * as ThongKeAction from '../actions/ThongKeAction'
import LayoutAdmin from '../components/AdminComponents/LayoutAdmin'
import HeaderAdmin from '../components/AdminComponents/HeaderAdmin'
class InternalAddressContainer extends React.Component {
    componentWillReceiveProps(nextProps) {
        if(nextProps.isLogout){
            this.props.actionLogout.resetLogout();
            this.props.history.push('/login');

        }
    }
    componentDidMount(){
        axios.get('http://localhost:3000/authentication')
        .then((response) => {
            if(response.data.status === 'true' && response.data.type === 1){
                const pageQuery = {
                    page : this.props.page_cur,
                    amount : 1
                };
                
                axios.post('http://localhost:3000/admin/getinforalladdressinternal', pageQuery)
                .then((result) =>{
                   
                    if(result.data.status === 'true'){
                        this.props.actions.getAllInternalAddressSuccess(result.data);
                    }
                    else{
                        this.props.actions.getAllInternalAddressFailure(result);
                    }
                }).catch((error) => {
                    console.log(error);
                });

            }
            else{
                this.props.history.push('/error');
            }
        })
    }

    onSearchAddress(){
        console.log("Address " + this.props.searchAddress);
        axios.get('http://localhost:3000/admin/getinforaddress/' + this.props.searchAddress)
        .then((response) =>{
            console.log("repone day", response);
            if(response.data.status === 'true'){
                console.log("ngon qua");
                this.props.actions.findAddressSuccess(response.data.balance);
            }
            else{
                this.props.actions.findAddressFailure(response);
            }
        }).catch((error)=>{
            console.log();
        });
    }
   
    onclickPrev(page){
        if(page > 1){
            this.props.actions.setPageCur(page - 1);
            this.componentDidMount();
        }
       
    }
    onclickNext(page){
        if(page < this.props.total_page){
            this.props.actions.setPageCur(page + 1);
            this.componentDidMount();
        }
        
    }

    render(){
        const {actions, listAddresses, page_cur, total_page, balances, searchAddress} = this.props;
        return(
            <div>
                 <HeaderAdmin
                onClickLogout={() => this.props.actionLogout.onClickLogout()}
                />
                
                <LayoutAdmin
                />
                
                <InternalAddress
                
                listAddresses = {listAddresses}
                page_cur = {page_cur}
                balances = {balances}
                setPageCur = {(value)=> actions.setPageCur(value)}
                total_page = {total_page}
                searchAddress = {searchAddress}
                onSearchAddress = {()=> this.onSearchAddress()}
                setSearchAddress = {(searchAddress) => actions.setSearchAddress(searchAddress)}
                onclickPrev = {(value) => {this.onclickPrev(value)}}
                onclickNext = {(value) => this.onclickNext(value)}
                />
            </div>
        )
    }
}

const mapStateToProps = state => ({
    listAddresses : state.InternalAddressReducer.address.listAddresses,
    page_cur: state.InternalAddressReducer.address.page_cur,
    total_page : state.InternalAddressReducer.address.total_page,
    balances : state.InternalAddressReducer.address.balances,
    searchAddress : state.InternalAddressReducer.address.searchAddress,
    isLogout: state.ThongKeReducer.ThongKe.isLogout,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(InternalAddressAction, dispatch),
    actionLogout : bindActionCreators(ThongKeAction, dispatch)

})


export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(InternalAddressContainer)