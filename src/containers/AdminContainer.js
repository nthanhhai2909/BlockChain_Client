import React from 'react'
import LayoutAdmin from '../components/AdminComponents/LayoutAdmin'
import HeaderAdmin from '../components/AdminComponents/HeaderAdmin'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import axios from 'axios'
import * as ThongKeAction from '../actions/ThongKeAction'
import { bindActionCreators } from 'redux'
class AdminContainer extends React.Component {
    componentWillReceiveProps(nextProps) {
        if(nextProps.isLogout){
            this.props.actions2.resetLogout();
            this.props.history.push('/login');

        }
    }
    
    async componentDidMount(){
        await axios.get('http://localhost:3000/authentication')
        .then(async(response) => {
            
            if(response.data.status === 'true' && response.data.type === 1){
            }
            else{
                this.props.history.push('/error');
            }
        })
    }
    render(){
        return(
            <div>
                <HeaderAdmin
                onClickLogout={() => this.props.actions2.onClickLogout()}
                />
                <LayoutAdmin/>
            </div>
        )
    }
}

const mapStateToProps = state => ({

    isLogout: state.ThongKeReducer.ThongKe.isLogout,
})

const mapDispatchToProps = dispatch => ({
    actions2: bindActionCreators(ThongKeAction, dispatch)
})

AdminContainer.propTypes = {
    

}

export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(AdminContainer)