import React from 'react'
import LayoutAdmin from '../components/AdminComponents/LayoutAdmin'
import HeaderAdmin from '../components/AdminComponents/HeaderAdmin'
import ListTransaction from '../components/AdminComponents/ListTransaction'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import * as ListTransactionAction from '../actions/ListTransactionAction'
import * as ThongKeAction from '../actions/ThongKeAction'
import axios from 'axios'

class ListTransactionContainer extends React.Component {
    componentWillReceiveProps(nextProps) {
        if(nextProps.isLogout){
            this.props.actions2.resetLogout();
            this.props.history.push('/login');

        }
    }
    async componentDidMount(){
        await axios.get('http://localhost:3000/authentication')
        .then(async(response) => {
            
            if(response.data.status === 'true' && response.data.type === 1){
                this.props.actions.loadListTransaction(this.props.page_cur);
            }
            else{
                this.props.history.push('/error');
            }
        })
    }
    
    render(){
        return(
            <div>
                <HeaderAdmin
                onClickLogout={() => this.props.actions2.onClickLogout()}
                />
                
                <LayoutAdmin
                />
                
                <ListTransaction 
                page_cur = {this.props.page_cur}
                total_page = {this.props.total_page}
                listTransaction = {this.props.listTransaction}
                onclickPrev = {(value) => {this.props.actions.onclickPrev(value)}}
                onclickNext = {(value) => this.props.actions.onclickNext(value)}
                handleKeyPress={(event) => this.props.actions.handleKeyPress(event)}
                />
            </div>
        )
    }
}


const mapStateToProps = state => ({
    page_cur: state.ListTransactionReducer.ListTransaction.page_cur,
    total_page: state.ListTransactionReducer.ListTransaction.total_page,
    listTransaction: state.ListTransactionReducer.ListTransaction.listTransaction,

    isLogout: state.ThongKeReducer.ThongKe.isLogout,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(ListTransactionAction, dispatch),
    actions2: bindActionCreators(ThongKeAction, dispatch)
})

ListTransactionContainer.propTypes = {
    

}

export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(ListTransactionContainer )