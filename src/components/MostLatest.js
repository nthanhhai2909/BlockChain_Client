import React, {Component} from 'react';
import PropTypes, { element } from 'prop-types';
import {Table, Pager} from 'react-bootstrap'
import FlatButton from 'material-ui/FlatButton';
import moment from 'moment-timezone';
const MostLatest = ({transaction, page_cur, total_page, handleKeyPress, onclickPrev, onclickNext}) =>(
    <div>
        <div style ={{float: "right", paddingBottom:5, paddingRight: 5}} className='Aligner'> 
            <input style={{type: 'text', width: 30,height: 30, fontSize: 20}}
                    onKeyPress={(event) => {
                        if (event.key === "Enter"){
                            handleKeyPress(event.target)
                        }
                    }}
                    defaultValue={page_cur}     
            />
            <span style={{ fontSize: 20 }}>/{total_page}</span>
            
        </div>
        
        
        <div>
            <table className="table table-responsive table-hover" >
                <thead>
                    <tr>
                        <th style={{width:"10%"}}>Stt</th>
                        <th style={{width:"10%"}}>Amount</th>
                        <th style={{width:"40%"}}>TransactionHasH</th>
                        <th style={{width:"20%"}}>Output index</th>
                        <th style={{width:"20%"}}>Output index</th>
                    </tr>
                </thead>
                <tbody>
                {
                    transaction.map((element, index) => {
                        let time = moment(element.date).tz('Asia/Ho_Chi_Minh').format('YYYY-MM-DD HH:mm:ss');
                        return (
                            <tr>
                                <td>{index + 1 + 10*(page_cur-1)}</td>
                                <td>{element.amount}</td>
                                <td>{element.hashTransactionID ==='undefined' ? 'Internal tranfer' : element.hashTransactionID}</td>
                                <td>{element.outputIndex}</td>
                                <td>{time}</td>
                               
                            </tr>
                        )
                    })
                }
                </tbody>
            </table >

            <Pager>
                <Pager.Item onClick={() =>{onclickPrev(page_cur)}} href="#" disabled={page_cur === 1}>&larr; Previous</Pager.Item>{'  '}
                <Pager.Item href="#" >{page_cur}</Pager.Item>{'  '}
                <Pager.Item onClick={() => onclickNext(page_cur)} href="#" disabled={page_cur === total_page}>Next &rarr;</Pager.Item>
            </Pager>
        </div>
        
    </div>
)

    

MostLatest.propTypes = {
    transaction: PropTypes.array.isRequired
}


export default MostLatest