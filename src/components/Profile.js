import React, { Component } from 'react';
import PropTypes from 'prop-types'
import Header from './Header'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
import { Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle } from 'material-ui/Toolbar'
import { Grid, Row, Col, Glyphicon, Button } from 'react-bootstrap'
import MostLatest from './MostLatest'
import Dialog from 'material-ui/Dialog'
import FromSent from './FormSent'
import FormSent from './FormSent';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

import TransactionManagement from './TransactionManagement'
const Profile = ({ actualBalance, availableBalance, address,
    isShowInfor, openShowInfor, closeShowInfor,
    setAddressTo, setAmountSent, setDescription,
    closeFromSent, setIsShowFromSent, isShow,
    submitFormSent, transaction, onclickLogout,
    handleKeyPress, handleClickTypeTransaction, isSelectDrop, total_page, page_cur,
    onclickPrev, onclickNext }) => (

        <div>
            <div>
                <Toolbar style={{ backgroundColor: '#15417D', height: 100, }}>
                    <ToolbarGroup firstChild={true}>
                        <FlatButton style={{ color: '#FAFAFA', fontSize: 30, marginLeft: 100, fontFamily: 'Sans-serif' }}
                        >
                            <span >BLOCKCHAIN</span>
                        </FlatButton>
                    </ToolbarGroup>
                    <ToolbarGroup >
                        <FlatButton style={{ color: '#FAFAFA', fontSize: 15, marginLeft: 100, fontFamily: 'Sans-serif' }}
                        >

                            <span onClick={() => { onclickLogout() }}>Logout</span>
                        </FlatButton>
                    </ToolbarGroup>
                </Toolbar>
            </div>
            <div>

                <div className="row" style={{ padding: 20, backgroundColor: '#FFFFFF' }}>
                    <div className="col-sm-12" >
                        <span style={{ fontSize: 20 }}>BE YOUR OWN BANK</span>
                        <Dialog
                            title="Information"
                            actions={[<FlatButton
                                label="OK"
                                primary={true}
                                onClick={() => closeShowInfor()}
                            />,]}
                            modal={true}
                            open={isShowInfor}
                            onRequestClose={() => closeShowInfor()}
                        >
                            Actual balance: {actualBalance} <br />
                            Available balance: {availableBalance}<br />
                            Address: {address}
                        </Dialog>

                        <div style={{ marginTop: 20 }}>
                        <Button
                            style={{ width: 100, marginRight: 10 }}
                            onClick={() => setIsShowFromSent(true)}
                        >SENT
                        </Button>

                        <Button style={{ width: 100, marginRight: 10 }}>RECEIVE</Button>
                        <Button
                            style={{ width: 100, marginRight: 10, width: 200 }}
                            onClick={() => openShowInfor()}
                        >YOUR INFORMATION
                                </Button>
                        <Button href="/user/transaction-management" style={{ width: 100, marginRight: 10, width: 250 }}>TRANSACTION MANAGEMENT</Button>

                        <div>
                            <DropDownMenu value={isSelectDrop} onChange={(event, index, value) => { handleClickTypeTransaction(value) }}>
                                <MenuItem value={1} primaryText="Receive" />
                                <MenuItem value={2} primaryText="Sent" />
                            </DropDownMenu>
                        </div>
                    </div>
                    <FormSent
                        setAddressTo={(value) => setAddressTo(value)}
                        setAmountSent={(value) => setAmountSent(value)}
                        setDescription={(value) => setDescription(value)}
                        closeFromSent={() => closeFromSent()}
                        isShow={isShow}
                        submitFormSent={() => submitFormSent()}
                    />
                    <div style={{ marginTop: 20 }}>
                        <MostLatest
                            transaction={transaction}
                            handleKeyPress={(event) => handleKeyPress(event)}
                            onclickPrev={(value) => { onclickPrev(value) }}
                            onclickNext={(value) => onclickNext(value)}
                            total_page={total_page}
                            page_cur={page_cur}
                        />
                    </div>

                    </div>
                </div>
            </div>
        </div>
        
        
    )

Profile.propTypes = {
    actualBalance: PropTypes.string,
    availableBalance: PropTypes.string,
    address: PropTypes.string,
    isSelectDrop: PropTypes.number,
    openShowInfor: PropTypes.func,
    closeShowInfor: PropTypes.func,
    isShowInfor: PropTypes.bool,
    setAmountSent: PropTypes.func,
    setAddressTo: PropTypes.func,
    setDescription: PropTypes.func,
    closeFromSent: PropTypes.func,
    setIsShowFromSent: PropTypes.func,
    isShow: PropTypes.bool,
    submitFormSent: PropTypes.func,
    transaction: PropTypes.array,
    onclickLogout: PropTypes.func,
    handleKeyPress: PropTypes.func,
    handleClickTypeTransaction: PropTypes.func,
    onclickNext: PropTypes.func,
    onclickPrev: PropTypes.func,
}


export default Profile