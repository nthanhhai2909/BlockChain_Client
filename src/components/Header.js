import React from 'react'
import {Link} from 'react-router-dom'
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
const Header = ({history}) => (
    <Toolbar style={{backgroundColor: '#1A237E', height: 100,}}>
        <ToolbarGroup firstChild={true}>
            <FlatButton style={{color: '#FAFAFA', fontSize: 30, marginLeft: 100, fontFamily:'Sans-serif'}}> 
                <span >BLOCKCHAIN</span>
            </FlatButton>
            
            <FlatButton 
                style={{color: '#FAFAFA', fontSize: 15, marginLeft: 50, fontFamily:'Sans-serif', }}
                    onClick={()=> history.push('/login')}
                > 
                <span >Login</span>
            </FlatButton>
        </ToolbarGroup>

        <ToolbarGroup>
        <RaisedButton  primary={true}
             style={{color: '#FAFAFA', fontSize: 15, marginLeft: 50, fontFamily:'Sans-serif', }}
             onClick={() => history.push('/register')}
             > 
                <span >GET A FREE WALLET</span>
            </RaisedButton>
        </ToolbarGroup>
        
    </Toolbar>
)
export default Header

