import React from 'react'
import {Link} from 'react-router-dom'
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
const HeaderDefaut = ({history}) => (
    <Toolbar style={{ backgroundColor: '#15417D', height: 100, }}>
        <ToolbarGroup firstChild={true}>
            <FlatButton style={{color: '#FAFAFA', fontSize: 30, marginLeft: 100, fontFamily:'Sans-serif'}}
                onClick={() => history.push('/')}
            > 
                <span >BLOCKCHAIN</span>
            </FlatButton>
        </ToolbarGroup>
    </Toolbar>
)
export default HeaderDefaut

