import React from 'react'
import {Row, Col, Grid} from 'react-bootstrap'
import HeadeDefaut from './HeaderDefaut'
import RaisedButton from 'material-ui/RaisedButton'
import PropTypes from 'prop-types'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
const ForgotPassword = ({history, setNewPassword, setConfirmPassword,
     isValidNewPassword, isValidConfirmPassword, submitForm, isReset, reset}) => (
    <div style={{backgroundColor: '#1A237E', height: 800}}>
        <HeadeDefaut
        history={history}
        />
        <Grid >
            <Row className="col-md-offset-3">
                <Dialog
                        title="NOTIFICATION"
                        actions={[<FlatButton
                            label="OK"
                            primary={true}
                            onClick={() => reset()}
                        />,]}
                        modal={true}
                        open={isReset}
                        >
                        Reset password success
                </Dialog>
                <Col md={8} style={{backgroundColor:'#FAFAFA'}}>
                    <span style={{ fontSize: 30, }}>Change password</span>
                    <br/>
                    
                    <hr/>

                    <div style={{ fontSize: 15, color:'#E65100' , textAlign: 'center'}}>
                        <span>
                            
                        </span>
                    </div>
                   

                    <div style={{ marginBottom:20}}>
                        <span style={{ fontSize: 15, }}>
                            New password
                        </span>
                        <br/>
                        <input type='password' style={{type: 'text', width: '100%', marginTop:5, height: 40, padding: 10}}
                            onChange={(event) => setNewPassword(event.target.value)}
                        />
                        <span style={{ fontSize: 15, color:'#E65100' , marginLeft: 20}}>
                            {
                                isValidNewPassword ? '' : 'invalid'
                            }
                        </span>
                    </div>
                    
                    <div style={{ marginBottom:20}}>
                        <span style={{ fontSize: 15, marginTop:20}}>
                            Confirm password
                        </span>
                        <br/>
                        <input type='password' style={{ width: '100%',  marginTop:5, height: 40, padding: 10}}
                            onChange={(event) => setConfirmPassword(event.target.value)}
                        />
                        <span style={{ fontSize: 15, color:'#E65100' ,marginLeft: 20}}>
                            {
                                isValidConfirmPassword ? '':'invalid'
                            }
                        </span>
                    </div>
                

                    <div style={{ marginBottom:20} }className='Aligner'>
                        <RaisedButton label="Submit" primary={true} onClick={() => submitForm()}/>
                    </div >

                </Col>
            </Row>
        </Grid>
    </div>
)

ForgotPassword.propTypes = {

    setNewPassword: PropTypes.func,
    setConfirmPassword: PropTypes.func,
    isValidNewPassword: PropTypes.bool,
    isValidConfirmPassword: PropTypes.bool,
    isReset: PropTypes.bool,
    submitForm: PropTypes.func,
    reset: PropTypes.func
}

export default ForgotPassword