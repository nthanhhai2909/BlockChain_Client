import React from 'react'
import Header from './Header'
const Home = ({history}) => (
    <div>
        <Header
            history={history}
        />
    </div>
)

export default Home