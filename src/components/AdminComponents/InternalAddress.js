import React, { Component } from 'react';
import PropTypes, { element } from 'prop-types';
import HeaderAdmin from './HeaderAdmin'
import LayoutAdmin from './LayoutAdmin'
import {Pager} from 'react-bootstrap'



const InternalAddress = ({ listAddresses, page_cur,
    total_page, balances, searchAddress, setSearchAddress, setPageCur, onSearchAddress,
    onclickPrev, onclickNext, onclickLogout }) => {

    return (
        <div>
            <section id="main-content">
                <section className="wrapper">
                    <div className="row">
                        <div className="col-lg-12">
                            <h3 className="page-header"><i className="fa fa-file-text-o" /> Form elements</h3>
                            <ol className="breadcrumb">
                                <li><i className="fa fa-home" /><a href="index.html">Home</a></li>
                                <li><i className="icon_document_alt" />Forms</li>
                                <li><i className="fa fa-file-text-o" />Form elements</li>
                            </ol>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12">
                            <section className="panel">
                                <header className="panel-heading">
                                    Form Elements
                                    </header>
                                <div className="panel-body">
                                    <form className="form-horizontal col-sm-4" onSubmit={(e) => { e.preventDefault(); onSearchAddress() }}>
                                        <div className="form-group">
                                            <input type="text" placeholder="Search for address here ..." minLength="64" maxLength="64"
                                                onChange={(e) => setSearchAddress(e.target.value)}
                                                value={searchAddress}
                                                required="true" className="form-control" />
                                        </div>
                                        <div className="form-group">
                                            <button className="btn btn-info">Search</button>
                                        </div>
                                    </form>
                                    <div className="col-sm-7 col-sm-offset-1" >
                                        <h4><span className="label label-info">
                                            {balances}
                                        </span></h4>
                                    </div>
                                </div>
                            </section>

                        </div>
                    </div>

                    <div className="row">
                        <div className="col-lg-12">
                            <section className="panel">
                                <header className="panel-heading">
                                    List Transactions
                                    </header>
                                <div className="panel-body">
                                    <div className="col-sm-12" >
                                        <table className="table table-responsive">
                                            <thead>
                                                <tr>
                                                    <th>User Id</th>
                                                    <th>Address</th>
                                                    <th>Actual Balances</th>
                                                    <th>Available Balances</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    listAddresses.map(element => {

                                                        return (
                                                            <tr>
                                                                <td>{element.userID}</td>
                                                                <td>{element.address}</td>
                                                                <td>{element.actualBalance}</td>
                                                                <td>{element.availableBalance}</td>
                                                                <td></td>
                                                            </tr>
                                                        )
                                                    }
                                                    )
                                                }
                                            </tbody>
                                        </table>
                                        <Pager>
                                            <Pager.Item onClick={() =>{onclickPrev(page_cur)}} href="#" disabled={page_cur === 1}>&larr; Previous</Pager.Item>{'  '}
                                            <Pager.Item href="#" >{page_cur}</Pager.Item>{'  '}
                                            <Pager.Item onClick={() => onclickNext(page_cur)} href="#" disabled={page_cur === total_page}>Next &rarr;</Pager.Item>
                                        </Pager>
                                    </div>
                                </div>
                            </section>

                        </div>
                    </div>
                </section>
            </section>
        </div>
    )
}



export default InternalAddress;