import React, {Component} from 'react';
import PropTypes, { element } from 'prop-types';
const LayoutAdmin = ({}) =>(

            
    <aside>
        <div id="sidebar" className="nav-collapse ">
            <ul className="sidebar-menu">
            <li className="active">
                <a className="" href="">
                    <i className="icon_house_alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li className="sub-menu">
                <a href="javascript:;" className="">
                    <i className="icon_documents_alt"></i>
                    <span>Report</span>
                    <span className="menu-arrow arrow_carrot-right"></span>
                </a>
                <ul className="sub">
                    <li><a className="" href="listuser">List Users</a></li>
                    <li><a className="" href="listtransaction"><span>List Transactions</span></a></li>
                    <li><a className="" href="thongke">Thống kê </a></li>
                    <li><a className="" href="internal-address">List address </a></li>
                </ul>
            </li>

            </ul>
        </div>
    </aside>
);

export default LayoutAdmin;