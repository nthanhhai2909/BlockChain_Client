import React, {Component} from 'react';
import PropTypes, { element } from 'prop-types';
import {Table, Pager} from 'react-bootstrap'
import FlatButton from 'material-ui/FlatButton';
const ListUser = ({listUser, page_cur, total_page, handleKeyPress, onclickPrev, onclickNext}) =>(
    <div>
        <section id="main-content">
            <section className="wrapper">
        <div style ={{float: "right", paddingBottom:5, paddingRight: 5}} className='Aligner'> 
            <input style={{type: 'text', width: 30,height: 30, fontSize: 20}}
                    onKeyPress={(event) => {
                        if (event.key === "Enter"){
                            handleKeyPress(event.target)
                        }
                    }}
                    defaultValue={page_cur}     
            />
            <span style={{ fontSize: 20 }}>/{total_page}</span>
            
        </div>
        
        
        <div style={{height:500}}>
            <Table striped bordered condensed hover  >
                <thead>
                    <tr>
                        <th style={{width:"10%"}}>Stt</th>
                        <th style={{width:"30%"}}>Email</th>
                        <th style={{width:"30%"}}>Actual Balance</th>
                        <th style={{width:"30%"}}>Available Balance</th>
                    </tr>
                </thead>
                <tbody>
                {
                    listUser.map((element, index) => {
                        
                        return (
                            <tr>
                                <td>{index + 1 + 10*(page_cur-1)}</td>
                                <td>{element.email}</td>
                                <td>{element.actualBalance}</td>
                                <td>{element.availableBalance}</td>
                               
                            </tr>
                        )
                    })
                }
                </tbody>
            </Table>

            <Pager>
                <Pager.Item onClick={() =>{onclickPrev(page_cur)}} href="#" disabled={page_cur === 1}>&larr; Previous</Pager.Item>{'  '}
                <Pager.Item href="#" >{page_cur}</Pager.Item>{'  '}
                <Pager.Item onClick={() => onclickNext(page_cur)} href="#" disabled={page_cur === total_page}>Next &rarr;</Pager.Item>
            </Pager>
        </div>
            </section>
        </section>
    </div>
)

    

ListUser.propTypes = {
    listUser: PropTypes.array.isRequired
}


export default ListUser