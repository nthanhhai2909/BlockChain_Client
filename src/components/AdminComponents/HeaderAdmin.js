import React, {Component} from 'react';

const Header = ({onClickLogout}) =>(

            <header className="header dark-bg">
                <div className="toggle-nav">
                    <div className="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i className="icon_menu"></i></div>
                </div>
                <a href="" className="logo">Transaction <span className="lite">Admin</span></a>

                <div className="top-nav notification-row">
                    <ul className="nav pull-right top-menu">
                  
                    <li className="dropdown">
                        <a data-toggle="dropdown" className="dropdown-toggle" href="#">
                            <span className="profile-ava">
                                <img alt="" src="img/avatar1_small.jpg" />
                            </span>
                            <span className="username">Jenifer Smith</span>
                            <b className="caret"></b>
                        </a>
                        <ul className="dropdown-menu extended logout">
                        <div className="log-arrow-up"></div>
                        <li className="eborder-top">
                            <a href="#"><i className="icon_profile"></i> My Profile</a>
                        </li>
                        <li>
                            <a onClick={() => onClickLogout()}><i className="icon_mail_alt"></i> Logout</a>
                        </li>
                       
                        </ul>
                    </li>
                    </ul>
                </div>
                </header>
        );


export default Header;