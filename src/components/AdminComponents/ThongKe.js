import React, {Component} from 'react';
import PropTypes, { element } from 'prop-types';
const ThongKe = ({numberUser, actualBalance, availableBalance}) =>(
    <section id="main-content">
        <section className="wrapper">
            <div className="row">
                <div className="col-sm-2">
                   <h2><span className="label label-success" >Số người dùng: </span></h2><br />
                   <h2><span className="label label-primary" >Số dư thực tế: </span></h2><br />
                   <h2><span className="label label-warning" >Số dư khả dụng: </span></h2><br />
                </div>
                <div className="col-sm-4">
                    <h2><span className="label label-success" >{numberUser}  </span></h2><br />
                    <h2><span className="label label-primary" >{actualBalance} </span></h2><br />
                    <h2><span className="label label-warning" >{availableBalance} </span></h2><br />
                </div>
            </div>
        </section>
    </section>
);

export default ThongKe;