import React, {Component} from 'react';
import PropTypes, { element } from 'prop-types';
import {Table, Pager} from 'react-bootstrap'
import FlatButton from 'material-ui/FlatButton';
const ListTransaction = ({listTransaction, page_cur, total_page, handleKeyPress, onclickPrev, onclickNext}) =>(
    <div>
        <section id="main-content">
            <section className="wrapper">
        <div style ={{float: "right", paddingBottom:5, paddingRight: 5}} className='Aligner'> 
            <input style={{type: 'text', width: 30,height: 30, fontSize: 20}}
                    onKeyPress={(event) => {
                        if (event.key === "Enter"){
                            handleKeyPress(event.target)
                        }
                    }}
                    defaultValue={page_cur}     
            />
            <span style={{ fontSize: 20 }}>/{total_page}</span>
            
        </div>
        
        
        <div style={{height:500}}>
            <Table striped bordered condensed hover  >
                <thead>
                    <tr>
                        <th style={{width:"10%"}}>Stt</th>
                        <th style={{width:"10%"}}>Amount</th>
                        <th style={{width:"20%"}}>Email</th>
                        <th style={{width:"30%"}}>TransactionHasH</th>
                        <th style={{width:"20%"}}>Output Index</th>
                        <th style={{width:"10%"}}>Status</th>
                    </tr>
                </thead>
                <tbody>
                {
                    listTransaction.map((element, index) => {
                        let status;
                        if (element.status === 1) {
                            status = 'Initalize';
                        }
                        else if (element.status === 2) {
                            status = 'Processing'
                        }
                        else {
                            status = 'Finish'
                        }
                        return (
                            <tr>
                                <td>{index + 1 + 10*(page_cur-1)}</td>
                                <td>{element.depositInformations[0].deposits}</td>
                                <td>{element.email}</td>
                                <td>{element.hashTransactionID ==='undefined' ? 'Internal tranfer' : element.hashTransactionID}</td>
                                <td>{element.outputIndex}</td>
                                <td>{status}</td>
                            </tr>
                        )
                    })
                }
                </tbody>
            </Table>

            <Pager>
                <Pager.Item onClick={() =>{onclickPrev(page_cur)}} href="#" disabled={page_cur === 1}>&larr; Previous</Pager.Item>{'  '}
                <Pager.Item href="#" >{page_cur}</Pager.Item>{'  '}
                <Pager.Item onClick={() => onclickNext(page_cur)} href="#" disabled={page_cur === total_page}>Next &rarr;</Pager.Item>
            </Pager>
        </div>
            </section>
        </section>
    </div>
)

    

ListTransaction.propTypes = {
    ListTransaction: PropTypes.array.isRequired
}


export default ListTransaction