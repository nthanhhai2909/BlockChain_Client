import React from 'react'
import HeaderDefaut from './HeaderDefaut'
import { Link } from 'react-router-dom'
import RaisedButton from 'material-ui/RaisedButton'
import PropTypes from 'prop-types'
import {
    Navbar, NavItem, NavDropdown, MenuItem, Nav, Row,
    Col, Grid, Pager, Button, InputGroup, FormControl
} from 'react-bootstrap'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import { Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle } from 'material-ui/Toolbar'
const Register = ({ history, setEmail, setPassword,
    setConfirm, changeAccpectTerm, isvalids,
    isRegister, submitForm, reset, isShowDialog }) => {
        return(
        <div className='register-form'>
            <Toolbar style={{ backgroundColor: '#15417D', height: 100, }}>
                <ToolbarGroup firstChild={true}>
                    <FlatButton style={{ color: '#FAFAFA', fontSize: 30, marginLeft: 100, fontFamily: 'Sans-serif' }}
                        onClick={() => history.push('/')}
                    >
                        <span onClick={() => reset()}>BLOCKCHAIN</span>
                    </FlatButton>
                </ToolbarGroup>
            </Toolbar>

            <div className="container">
                <div className="row">
                    <div className="col-sm-6 col-sm-offset-3" style={{ padding: 20, backgroundColor: '#FFFFFF' }}>
                        <form id="sign-up" onSubmit={(e) => {e.preventDefault(); submitForm() }}>

                            <Dialog
                                title="NOTIFICATION"
                                actions={[<FlatButton
                                    label="OK"
                                    primary={true}
                                    onClick={() => reset()}
                                />,]}
                                modal={true}
                                open={isShowDialog}
                                onRequestClose={() => reset()}
                            >
                                Sign Up Success, Please check email!!!
                            </Dialog>
                            <div className="row">
                                <div className="col-sm-7">
                                    <span className="subject">Create Your Wallet</span>
                                    <p><strong>Sign up for free wallet below</strong></p>
                                </div>
                                <div className="col-sm-5">
                                <span>or <Link className="btn btn-warning" to="/login"><strong>Login</strong></Link></span>
                                </div>

                            </div>

                            <div className="row">
                                <div className="col-sm-12">
                                    <hr />
                                    {
                                        isRegister ? (
                                            ""
                                        ) :
                                            (
                                                <div>
                                                    <p className="text-center alert alert-danger">User Already exsit</p>
                                                </div>
                                            )
                                    }
                                    {
                                        isvalids.isValidConfirm ? (
                                            ""
                                        ) :
                                            (
                                                <div>
                                                    <p className="text-center alert alert-danger">Password confirmation is not match</p>
                                                </div>
                                            )
                                    }
                                    
                                    
                                </div>
                                <div className="col-sm-12">
                                    <div className="form-group">
                                        <label>Email</label>
                                        <input onChange={(event) => setEmail(event.target.value)} type="email"
                                        required="true"
                                        
                                         className="form-control" />
                                    </div>

                                    <div className="form-group">
                                        <label>Password</label>
                                        <input 
                                        required="true"
                                        minLength="6"
                                        onChange={(event) => setPassword(event.target.value)} type="password" className="form-control" />
                                    </div>

                                    <div className="form-group">
                                        <label>Confirm Password</label>
                                        <input 
                                        required="true"
                                        minLength="6"
                                        onChange={(event) => setConfirm(event.target.value)} type="password" className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <button className="btn btn-info btn-block">SIGN UP</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    )
}

Register.propTypes = {
    setEmail: PropTypes.func.isRequired,
    setPassword: PropTypes.func.isRequired,
    setConfirm: PropTypes.func.isRequired,
    changeAccpectTerm: PropTypes.func.isRequired,
    isvalids: PropTypes.objectOf(PropTypes.bool).isRequired,
    isRegister: PropTypes.bool.isRequired,
    submitForm: PropTypes.func.isRequired,
    reset: PropTypes.func.isRequired,
    isShowDiglog: PropTypes.bool.isRequired
}

export default Register