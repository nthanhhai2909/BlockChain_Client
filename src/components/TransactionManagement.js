import React from 'react'
import PropTypes from 'prop-types'
import { Table } from 'react-bootstrap'
import Action from './Action'
import Header from './Header'
import { Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle } from 'material-ui/Toolbar'
import { Grid, Row, Col, Glyphicon, Button } from 'react-bootstrap'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
import { Link } from 'react-router-dom'
import {Pager} from 'react-bootstrap'
const TransactionManagement = ({page_cur, total_page, transaction, onclickLogout, revokeTransaction, onclickPrev, onclickNext }) => {

    return (
        <div>
            <div>
                <Toolbar style={{ backgroundColor: '#15417D', height: 100 }}>
                    <ToolbarGroup firstChild={true}>
                        <FlatButton style={{ color: '#FAFAFA', fontSize: 30, marginLeft: 100, fontFamily: 'Sans-serif' }}
                            href="/profile">
                            <span >BLOCKCHAIN</span>
                        </FlatButton>
                    </ToolbarGroup>
                    <ToolbarGroup >
                        <FlatButton style={{ color: '#FAFAFA', fontSize: 15, marginLeft: 100, fontFamily: 'Sans-serif' }}
                        >

                            <span onClick={() => onclickLogout()}>Logout</span>
                        </FlatButton>
                    </ToolbarGroup>
                </Toolbar>
            </div>
            <div style={{ maxHeight: 500, marginTop: 20 }}>
                <h3>Transaction Management</h3>
                <table className="table table-responsive table-hover" >
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Address to</th>
                            <th>Amounts</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {

                            transaction.map(element => {
                                let status;
                                if (element.status === 1) {
                                    status = 'Initalize';
                                }
                                else if (element.status === 2) {
                                    status = 'Processing'
                                }
                                else {
                                    status = 'Finish'
                                }
                                return (
                                    <tr>
                                        <td>{element.id}</td>
                                        <td>{element.addressTo}</td>
                                        <td>{element.amount}</td>
                                        <td>{status}</td>
                                        <td>
                                            {
                                                (element.status === 1)
                                                    ? (
                                                        <button onClick={() => revokeTransaction(element.id)} className="btn btn-danger">Revoke</button>
                                                    )
                                                    :
                                                    ""
                                            }
                                        </td>
                                    </tr>
                                )
                            }
                            )
                        }
                    </tbody>
                </table>
                <Pager>
                    <Pager.Item onClick={() => { onclickPrev(page_cur) }} href="#" disabled={page_cur === 1}>&larr; Previous</Pager.Item>{'  '}
                    <Pager.Item onClick={() => onclickNext(page_cur)} href="#" disabled={page_cur === total_page}>Next &rarr;</Pager.Item>
                </Pager>
            </div>
        </div>

    )
}


TransactionManagement.propTypes = {
    transaction: PropTypes.array
}

export default TransactionManagement