import React from 'react'
import HeaderDefaut from './HeaderDefaut'
import {Row, Col, Grid, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'
const ConfirmSent = ({backhome}) => (
    <div>
        <HeaderDefaut/>
        <div style={{backgroundColor:'#1A237E', height: 800,}}>
            <Grid>
                <Row className="col-md-offset-3">
                    <Col md={8} style={{backgroundColor:'#FAFAFA', marginTop: 100}}>
                        <h3 style={{textAlign:'center'}}>You have successfully validated</h3>
                        <div style={{textAlign:'center', fontSize: 20}}>
                            <Button onClick={() => backhome()}>Back Profile</Button>
                        </div>
                        
                    </Col>
                </Row>
            </Grid>
        </div>
    </div>
)

export default ConfirmSent