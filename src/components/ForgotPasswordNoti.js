import React from 'react'
import HeaderDefaut from './HeaderDefaut'
import {Row, Col, Grid} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import PropTypes from 'prop-types'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar'
const ForgotPasswordNoti = ({isForGot, submitForm,setEmailReset, resetForm, backHome}) => (
    <div>
        <Toolbar style={{backgroundColor: '#1A237E', height: 100,}}>
            <ToolbarGroup firstChild={true}>
                <FlatButton style={{color: '#FAFAFA', fontSize: 30, marginLeft: 100, fontFamily:'Sans-serif'}}
                    onClick={() => backHome()}
                > 
                    <span >BLOCKCHAIN</span>
                </FlatButton>
            </ToolbarGroup>
        </Toolbar>
        <div style={{backgroundColor:'#1A237E', height: 800,}}>
        {
            isForGot ? 
            <Grid>
                <Row className="col-md-offset-3">
                    <Col md={8} style={{backgroundColor:'#FAFAFA', marginTop: 100}}> 
                        <h4>An email has been sent to your email. please check email. The content of the link contains your password</h4> 
                        <div className='Aligner'>
                            <Link to='/login' onClick={() => resetForm()}>Login</Link>
                        </div>
                        
                    </Col>
                </Row>
            </Grid>
            :
            <Grid>
            <Row className="col-md-offset-3">
                <Col md={8} style={{backgroundColor:'#FAFAFA', marginTop: 100}}>
                <div style={{ fontSize: 15, color:'#E65100' , textAlign: 'center'}}>

                </div>
                <div>
                    <span style={{ fontSize: 30, }}>Reset password</span>
                    <hr/>
                
                </div>
                    <div style={{ marginBottom:20, }}>
                        <span style={{ fontSize: 15, }}>
                            Email
                        </span>
                        <br/>
                        <input style={{type: 'text', width: '100%', marginTop:5, height: 40, padding: 10}}
                            onChange={(e) => setEmailReset(e.target.value)}
                        />    
                    </div>
                    <div className='Aligner'>
                        <RaisedButton label="Submit" primary={true} style={{marginBottom:30}}
                            onClick={() => submitForm()}/>
                    </div>   
                </Col>
            </Row>
        </Grid>

        }
        </div>
    </div>
)


ForgotPasswordNoti.propTypes = {
    isForGot: PropTypes.bool,
    submitForm: PropTypes.func,
    setEmailReset: PropTypes.func,
    resetForm: PropTypes.func,
    backHome: PropTypes.func,
}


export default ForgotPasswordNoti