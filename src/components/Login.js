import React from 'react'
import HeaderDefaut from './HeaderDefaut'
import { Link } from 'react-router-dom'
import RaisedButton from 'material-ui/RaisedButton'
import PropTypes from 'prop-types'
import {
    Navbar, NavItem, NavDropdown, MenuItem, Nav, Row,
    Col, Grid, Pager, Button, InputGroup, FormControl
} from 'react-bootstrap'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'

const Login = ({ history, setEmail, setPassword, submitForm, isvalids, message }) => (
    <div className='register-form'>
        <HeaderDefaut style={{ backgroundColor: '#15417D', height: 100, }}
            history={history}
        />



        <div className="container">
            <div className="row">
                <div className="col-sm-6 col-sm-offset-3" style={{ padding: 20, backgroundColor: '#FFFFFF' }}>
                    <form id="sign-up" onSubmit={(e) => { e.preventDefault(); submitForm() }}>
                        <div className="row">
                            <div className="col-sm-7">
                                <span className="subject">Welcome Back</span>
                                <p><strong>SignIn in the form below</strong></p>
                            </div>
                            <div className="col-sm-5">
                                <span>or <Link className="btn btn-warning" to="/register"><strong>Sign Up</strong></Link></span>
                            </div>


                            
                        </div>

                        <div className="row">
                        <div className="col-sm-12">
                                
                                <hr />
                                <p>
                                    {message === '' ? '' : <p className="text-center alert alert-danger"> {message}</p>}
                                    {isvalids.isValidEmail ? '' : <p className="text-center alert alert-danger"> Email invalid</p>}
                                    {isvalids.isValidPassword ? '' : <p className="text-center alert alert-danger"> Password invalid</p>}
                                </p>
                                
                            </div>
                            <div className="col-sm-12">
                                    <div className="form-group">
                                        <label>Email</label>
                                        <input required="true"
                                            type="email" onChange={(event) => setEmail(event.target.value)} className="form-control" />
                                    </div>

                                    <div className="form-group">
                                        <label>Password</label>
                                        <input minLength="6" onChange={(event) => setPassword(event.target.value)} type="password" className="form-control" />
                                    </div>

                                    <div className="form-group">
                                        <span><Link to="/forgot">Forgot Password</Link></span>
                                    </div>

                                    <div className="form-group">
                                        <button className="btn btn-info btn-block">LOG IN</button>
                                    </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>


)

Login.propTypes = {

}

export default Login