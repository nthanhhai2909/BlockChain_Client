import React from 'react'
import PropTypes from 'prop-types'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'
import { Button } from 'react-bootstrap'
const FromSent = ({setAmountSent, setAddressTo, setDescription,
    closeFromSent, setIsShowFromSent, isShow,
    submitFormSent}) => {
    const actions = [
        <FlatButton
          label="Cancel"
          primary={true}
          onClick={() => closeFromSent()}
        />,
        <FlatButton
          label="Submit"
          primary={true}
          keyboardFocused={true}
          onClick={() => submitFormSent()}
        />,
      ];
      return(
        <div>
            <Dialog
            title="SENT BITCOIN"
            actions={actions}
            modal={false}
            open={isShow}
            onRequestClose={() => closeFromSent()}
            >
                <div>
                    <span>Address to</span>
                    <br/>
                    <TextField
                        onChange={(e) => setAddressTo(e.target.value)}
                        fullWidth={true}
                        //errorText={isValidIDWalletSent ? '' : 'INVALID'}
                    />
                    <br/>
                    <br/>
                    <span>Amount of money</span>
                    <br/>
                    <TextField
                        onChange={(e) => setAmountSent(e.target.value)}
                        fullWidth={true}
                        //errorText={isValidAmountSent ? '' : 'INVALID'}
                    />
                    <br/>
                </div>
            </Dialog>
        </div>
      )
}

FromSent.propTypes = {
    setAmountSent: PropTypes.func,
    setAddressTo: PropTypes.func,
    setDescription: PropTypes.func,
    setIsShowFromSent: PropTypes.func,
    closeFromSent: PropTypes.func,
    isShow: PropTypes.bool,
    submitFormSent: PropTypes.func
}

export default FromSent